# Translation of plasmanetworkmanagement-kcm.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasmanetworkmanagement-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-19 02:23+0000\n"
"PO-Revision-Date: 2017-10-30 23:08+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

# >> @title:window
#: kcm.cpp:285
#, fuzzy, kde-format
#| msgid "Import VPN Connection"
msgid "Failed to import VPN connection: %1"
msgstr "Увоз ВПН везе"

# well-spelled: моја_дељена_веза
#: kcm.cpp:355
#, kde-format
msgid "my_shared_connection"
msgstr "моја_дељена_веза"

# >> @title:window
#: kcm.cpp:427
#, kde-format
msgid "Export VPN Connection"
msgstr "Извоз ВПН везе"

#: kcm.cpp:447
#, kde-format
msgid "Do you want to save changes made to the connection '%1'?"
msgstr "Желите ли да сачувате измене над везом „%1“?"

#: kcm.cpp:448
#, kde-format
msgctxt "@title:window"
msgid "Save Changes"
msgstr "Уписивање измена"

# >> @title:window
#: kcm.cpp:543
#, kde-format
msgid "Import VPN Connection"
msgstr "Увоз ВПН везе"

# >> @title:window
#: kcm.cpp:545
#, fuzzy, kde-format
#| msgid "Export VPN Connection"
msgid "VPN connections (%1)"
msgstr "Извоз ВПН везе"

#: kcm.cpp:548
#, kde-format
msgid "No file was provided"
msgstr ""

#: kcm.cpp:597
#, kde-format
msgid "Unknown VPN type"
msgstr ""

#: qml/AddConnectionDialog.qml:15
msgctxt "@title:window"
msgid "Choose a Connection Type"
msgstr "Избор типа везе"

#: qml/AddConnectionDialog.qml:162
msgid "Create"
msgstr "Направи"

#: qml/AddConnectionDialog.qml:173 qml/ConfigurationDialog.qml:117
msgid "Cancel"
msgstr "Одустани"

#: qml/AddConnectionDialog.qml:190 qml/main.qml:184
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:15
msgctxt "@title:window"
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:37
msgid "General"
msgstr ""

#: qml/ConfigurationDialog.qml:42
msgid "Ask for PIN on modem detection"
msgstr ""

#: qml/ConfigurationDialog.qml:49
#, fuzzy
#| msgid "Export selected connection"
msgid "Show virtual connections"
msgstr "Извези изабрану везу"

#: qml/ConfigurationDialog.qml:57
msgid "Hotspot"
msgstr ""

#: qml/ConfigurationDialog.qml:63
msgid "Hotspot name:"
msgstr ""

#: qml/ConfigurationDialog.qml:73
msgid "Hotspot password:"
msgstr ""

#: qml/ConfigurationDialog.qml:102
msgid "OK"
msgstr ""

#: qml/ConnectionItem.qml:107
msgid "Connect"
msgstr "Повежи се"

#: qml/ConnectionItem.qml:107
msgid "Disconnect"
msgstr "Прекини везу"

#: qml/ConnectionItem.qml:120
msgid "Delete"
msgstr "Обриши"

#: qml/ConnectionItem.qml:130
msgid "Export"
msgstr "Извези"

# >> @item
#: qml/ConnectionItem.qml:140
msgid "Connected"
msgstr "повезан"

# >> @item
#: qml/ConnectionItem.qml:142
msgid "Connecting"
msgstr "у повезивању"

#: qml/main.qml:130
msgid "Add new connection"
msgstr "Додај нову везу"

#: qml/main.qml:144
msgid "Remove selected connection"
msgstr "Уклони изабрану везу"

#: qml/main.qml:160
msgid "Export selected connection"
msgstr "Извези изабрану везу"

#: qml/main.qml:211
msgctxt "@title:window"
msgid "Remove Connection"
msgstr "Уклањање везе"

#: qml/main.qml:212
msgid "Do you want to remove the connection '%1'?"
msgstr "Желите ли заиста да уклоните везу „%1“?"
